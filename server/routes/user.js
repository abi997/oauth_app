const express = require('express')
const passport= require('passport')
const cp = require('child_process')
const User = require('../models/User');
const { stdout, stderr } = require('process');
const router = express.Router();
require('../passport/config')
// ------
router.get('/',(req,res)=>{
  const child = cp.exec('ls',{cwd:""},(error,stdout,stderr)=>{
    if (error) {
      console.log(`exec error:\n${error}`);
      return;
    }
    else{
     if(stdout){
      console.log(`stdout:\n${stdout}`);
       return ;
      //  return res.send(stdout)
     }
     console.log(`stderr:\n${stderr}`);
     return;
    //  return res.send(stderr)
    }
  })
});
// ------
router.post('/signup',async(req,res)=>{
 
  const {email,password} = req.body;
  try {
     
    let user = await User.findOne({email})
    if(user){
      return res.status(401).json({error:'user already exist'})
    }
 
  //db saving
    user = new User({
      methods:'local',
      local:{
        email,
        password
      }
    })
  await user.save();

 const token = await user.generateToken()

 return res.status(201)
   .json({
      success:true, 
      message:'succesfull registerd',
      email,
      token
    })  
  }

catch (error) {
     res.status(500).json({
       success:false,
      error:error.message
    })
  }

});


// login
router.post('/local/login',async(req,res)=>{  

    const {email,password} = req.body;

  try{
    
   let user = await User.findByCredtionals(email,password)
  
   if(!user){
     return res.status(401).json({
       success:false,
       error:'Unable to login',
    
      })
   }
    
    let token =await user.generateToken()
    
   return res 
       .status(200)
       .json({
         success:true,
         message:'Successfully login',
         email,
         token 
        })
  }
  catch(error){
  
    return res.status(500).json({
      success:false,
      error:'server error'})
    }
  });

router.get('/loginPage',(req,res)=>{
  return res.status(400).send({error:"Something Went Wrong!"})
});

//----------------------google-------------------------

router.get('/google/login',
         passport.authenticate('google',{scope:['profile','email']}));  


router.get('/google/login/callback',
         passport.authenticate('google',{failureRedirect:'/loginPage'}),
         async(req,res)=>{
           
           const user = req.user;

           let token =await user.generateToken()
    
           return res 
               .status(200)
               .json({
                 success:true,
                 message:'Successfully login',
                 token 
                })
         });



  //---------------------------Linkedin------------------------ 
router.get('/linkedin/login',
  passport.authenticate('linkedin'));


router.get('/linkedin/login/callback', 
         passport.authenticate('linkedin', {failureRedirect: '/loginPage'}),
    async(req,res)=>{
      const user = req.user;

      let token =await user.generateToken()

      return res 
          .status(200)
          .json({
            success:true,
            message:'Successfully login',
            token 
          })
}
);

// -----------------------------Slack---------------------

router.get('/slack/login', passport.authorize('slack'));

// OAuth callback url
router.get('/slack/login/callback', 
  passport.authorize('slack', { failureRedirect: '/loginPage' }),
  async(req,res)=>{
  
    const user = req.account;

    let token =await user.generateToken()

    return res 
        .status(200)
        .json({
          success:true,
          message:'Successfully login',
          token 
         })
  });
                 
module.exports= router;