
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


//Schema
const userSchema = new mongoose.Schema({

  methods:{
    type:String,
    required:true
  },
  local:{
    email:{
      type:String,
      lowercase:true
    },
    password:{
       type:String
    }
  },
  google:{
    id:{
      type:String
    },
    email:{
      type:String,
      lowercase:true
    }
  },
  slack:{
    id:{
      type:String
    },
    email:{
      type:String,
      lowercase:true
    }
  },
  linkedin:{
    id:{
      type:String
    },
    email:{
      type:String,
      lowercase:true
    }
  },
  tokens:[{
      token:{
        type:String,
      }
  }]
});

//token generate
userSchema.methods.generateToken = async function(){
  const user = this
  const token = jwt.sign({_id: user._id.toString()},process.env.JWT_SECRET)
  user.tokens = user.tokens.concat({ token })
  await user.save()

  return token
}

// hash generate 
userSchema.pre('save',async function (req,res,next){  
  try{
    const user = this;
    if(user.methods !== 'local'){
      next()
    };
    
  //hashed checking once
    if(user.isModified('local.password')) {
        user.local.password =  await bcrypt.hash(user.local.password,8)
      }
    next();
  }
  catch(error){
    console.log(error) 
    next(error)
  }
      
  });

//login credtionals
userSchema.statics.findByCredtionals=async(email,password)=>{
  const user = await User.findOne({'local.email':email})
  
  if(!user){
    return user
  };
  const passwordVerify = await bcrypt.compare(password,user.local.password)
 
  if(!passwordVerify){
    return passwordVerify
  };
   return user;
 }

const User = mongoose.model('User',userSchema);

module.exports = User;