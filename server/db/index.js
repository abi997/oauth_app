const mongoose = require('mongoose')



async function ConnectDB(){
 try{
  await mongoose.connect(process.env.URI,{useCreateIndex:true,useUnifiedTopology:true,useNewUrlParser:true})
  console.log('connected Db')
}
 catch(error){
   console.log(error.message)
   process.exit(1)
 }
}

module.exports=ConnectDB;