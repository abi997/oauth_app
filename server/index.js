const express = require('express')
const passport = require('passport')
const cp = require('child_process')
const app = express();
const ConnectDB = require('./db')
ConnectDB()

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(passport.initialize());
app.use(passport.session())

app.use('/oauth',require('./routes/user'));

/*
app.post('/signup',(req,res)=>{
  const childProcess = cp.fork('./routes/signUp.js')
  childProcess.send({"data":req.body})
  childProcess.on('message',message=>res.send(message))
  childProcess.on('exit',()=>{
    console.log('child terminated')
  })
})

app.post('/login',(req,res)=>{
  const childProcess = cp.fork('./routes/login.js')
  childProcess.send({"data":req.body})
  childProcess.on('message',message=>res.send(message))
  childProcess.on('exit',()=>{
    console.log('child terminated')
  })
})
*/

const PORT =  process.env.PORT || 5000

app.listen(PORT,()=>{
  console.log(`Server Is Up ${PORT}`)
})
