
let  passport = require('passport')
const {Strategy} = require('passport-google-oauth20');
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
const SlackStrategy = require('passport-slack').Strategy
const User = require('../models/User')


const Google_Client=process.env.GOOGLE_Client;
const Google_Key=process.env.GOOGLE_Key;
const Linkedin_Client=process.env.LINKEDIN_CLIENT;
const Linkedin_Key=process.env.LINKEDIN_Key;
const Slack_Client=process.env.SLACK_CLIENT;
const Slack_Key=process.env.SLACK_KEY;

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});
//------------------------Google-----------------------------------------------
passport.use(new Strategy({

  clientID:Google_Client,
  clientSecret:Google_Key,
  callbackURL:'/google/login/callback'
  
},async (accessToken, refershToken, profile, done)=> {
 try {

    let existingUser = await User.findOne({'google.id':profile.id});
    if(existingUser){
      return done(null,existingUser)
    }


    //newUser
    const newUser= new User({
      methods:'google',
      google:{
        id:profile.id,
        email:profile.emails[0].value
      }
    })

    await newUser.save();
    done(null,newUser);
  } catch (error) {
    done(error,false,error.message)
  }
}));

//----------------------------------Linkedin-------------------------


passport.use(new LinkedInStrategy({
  clientID: Linkedin_Client,
  clientSecret: Linkedin_Key,
  callbackURL: "http://localhost:5000/linkedin/login/callback",
  scope: ['r_emailaddress','r_liteprofile'],
},
async (accessToken, refershToken, profile, done)=> {
  
  try {
    console.log("accesstoken",accessToken)
    let existingUser = await User.findOne({'linkedin.id':profile.id});
    if(existingUser){
      return done(null,existingUser)
    }


    //newUser
    const newUser= new User({
      methods:'linkedin',
      linkedin:{
        id:profile.id,
        email:profile.emails[0].value
      }
    })

    await newUser.save();
    done(null,newUser);
  } catch (error) {
    done(error,false,error.message)
  }
}));

//------------------------------Slack-----------------------------------------------
 
passport.use(new SlackStrategy({
    clientID:Slack_Client,
    clientSecret:Slack_Key
  }, async(accessToken, refreshToken, profile, done) => {
    
  
    try {
      console.log(profile.user.id)
      let existingUser = await User.findOne({'slack.id':profile.user.id});
      if(existingUser){
        return done(null,existingUser)
      }
  
  
      //newUser
      const newUser= new User({
        methods:'slack',
        slack:{
          id:profile.user.id,
          email:profile.user.email
        }
      })
  
      await newUser.save();
      done(null,newUser);
    } catch (error) {
      done(error,false,error.message)
    }
  }));

 
